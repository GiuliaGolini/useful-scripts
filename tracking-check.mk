# Find the input images that have tracking issues.
#
# USAGE: Set the high-level configuration at the start, then run the
# -----  command below which will be run on 12 threads. Instead of
#        '12' use any number of threads that you want to further speed
#        up the processing (recall that you can find the number of
#        threads on GNU/Linux operating systems with the 'nproc'
#        comman).
#
#   make -f tracking-check.mk -j12
#
# OUTPUT: Two plain text files:
# ------
#        use.txt: list of files that don't have tracking issues.
#        no-use.txt: list of files that have tracking issues.
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2020 Mohammad Akhlaghi.
#
# This Makefile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# For a copy of the GNU General Public License see
# <http://www.gnu.org/licenses/>.





# IDEAS FOR FUTHER IMPROVEMENT
# ----------------------------
#
# * Use a higher pixel threshold for the axis-ratio measurement. The
#   current 1-sigma pixel threshold was mainly defined for the
#   center-difference check, where the stars can have a bright center,
#   with only a slight sudden push (thin track) and its good for
#   that. But for the axis-ratio measurement, 1-sigma is low and
#   increasing it will help separate the tracking error images better.





# Configuration
# -------------
#
# The suffix of the images to use for testing.
SUFFIX = .fit

# Directory to build temporary intermediate files. After this Makefile
# finishes, you can safely delete this temporary directory (of course,
# in case there is no problem that you would like to debug).
BDIR = /scratch/build/tracking-check

# Input data directory.
INDIR = /datasets/images/high-level/javier-check-tracking/tests-second

# Pixel-S/N threshold. This S/N threshold is applied to the pixels of
# the image to define objects.
PIX-SN-THRESH = 1

# Object S/N threshold: the minimum S/N of the objects (containing
# multiple pixels) to use for the objects to use for the axis-ratio
# and center-difference thresholds.
SN-THRESH = 100

# Axis ratio threshold: the minimum acceptable median axis-ratio.
Q-THRESH = 0.71

# Center-difference threshold: the maximum accepable number of pixels
# between the geometric center and brightness-weighted center.
CD-THRESH = 5





# Basic Make settings.
all: use.txt
.ONESHELL:
SHELL = /bin/bash
.SHELLFLAGS = -ec





# Find the list of input images: with the 'wildcard' function, we are
# getting the full address list, then we are removing the '$(INDIR)'
# and '$(SUFFIX)' components so we have raw names with no directory or
# suffix information, helping in defining the targets below.
list := $(subst $(SUFFIX),, \
          $(subst $(INDIR)/,, \
           $(wildcard $(INDIR)/*$(SUFFIX))))





# The directories keeping the temporary results of each image.
imgdirs=$(foreach l,$(list),$(BDIR)/$(l))
$(imgdirs): | $(BDIR); mkdir $@
$(BDIR):; mkdir $@





# Do the basic detection to estimate the noise level.
#list=$(shell seq $$(wc -l class.txt | awk '{print $$1}'))
nc=$(foreach l,$(list),$(BDIR)/$(l)/thresh.txt)
$(nc): $(BDIR)/%/thresh.txt: $(INDIR)/%$(SUFFIX) | $(BDIR)/%

        # Set the temporary file names.
	thresh=$(subst .txt,.fits,$@)
	sky=$(subst .txt,_sky.fits,$@)

        # Use the '--sky' feature of Gnuastro's Statistics program to
        # estimate the "sky" and its standard deviation.
	aststatistics $< -h0 --sky -o$@

        # Apply a pixel-S/N threshold, erode it (to remove false/small
        # peaks) and label each separate group of connected pixels
        # into one labeled image that will be used in the next rule to
        # do measurements.
	astarithmetic $< -h0 $$sky -hSKY - $$sky -hSKY_STD / \
	              $(PIX-SN-THRESH) gt 1 erode 2 connected-components \
	              -o$$thresh

        # Make an empty final "target" for this rule (the main files
        # created are large images which will be deleted in the next
        # step to avoid wasting space).
	touch $@



# Do the measurements over the regions.
cat=$(subst thresh.txt,cat.fits,$(nc))
$(cat): $(BDIR)/%/cat.fits: $(BDIR)/%/thresh.txt

        # Set the names of the temporary files from the previous step.
	thresh=$(BDIR)/$*/thresh.fits
	sky=$(BDIR)/$*/thresh_sky.fits

        # Generate the catalog.
	astmkcatalog $$thresh -h1 --ids --x --y --geox --geoy \
	             --geoaxisratio --sn -o$@ \
	             --valuesfile=$(INDIR)/$*$(SUFFIX) --valueshdu=0 \
	             --insky=$$sky --skyhdu=SKY --subtractsky \
	             --instd=$$sky --stdhdu=SKY_STD

        # Delete the temporary files from the previous step.
	rm $$sky $$thresh





# Do the basic measurements of on each image and write them into a
# one-line file that will be used in the end.
result=$(subst cat.fits,result.txt,$(cat))
$(result): $(BDIR)/%/result.txt: $(BDIR)/%/cat.fits

        # If a 'class.txt' file exists, we are de-bugging mode and
        # want to see what the pre-defined class for this image is.
	class="no-class"
	if [ -f class.txt ]; then
	  class=$$(awk 'BEGIN{out=""} \
	                !/^$*/{out=$$2} \
	                END{if(out=="") print "no-class"}' \
	                class.txt)
	fi

        # Calculate the median axis ratio of all the high
        # signal-to-noise objects.
	q=$$(asttable $< --range=SN,$(SN-THRESH),inf \
	              -cGEO_AXIS_RATIO \
	              | aststatistics --median)

        # Calculate the difference between the geometric center of the
        # objects their brightness-weighted center, then calculate the
        # median of the 10 with the largest difference.
	check=$(BDIR)/$*/check.fits
	asttable $< --range=SN,100,inf -o$$check \
	         -cOBJ_ID -c'arith X GEO_X - X GEO_X - x \
	                           Y GEO_Y - Y GEO_Y - x + sqrt'
	r=$$(asttable $$check -c2 --sort=2 \
	              | tail -n10 \
	              | aststatistics --median)

        # If the median axis ratio is less than 0.5 or the median
        # center difference is more than 5 pixels, then classify this
        # object as a 'no-use'. Otherwise, it should have a good
        # tracking.
	result=$$(echo $$q $$r \
	  | awk '{if($$1<$(Q-THRESH) || $$2>$(CD-THRESH)) \
	              print "no-use"; \
	         else print "use" }')

        # Write all the necessary information into a file.
	printf "%-20s$(SUFFIX) %.2f %.2f %-7s %s\n" \
	       $* $$q $$r $$result $$class > $@





# Do the final classification: the ones that don't have tracking
# errors will be put into the 'use.txt' file and the ones that have
# tracking errors will be put in 'no-use.txt'.
use.txt: $(result)
	cat $(result) | awk '$$4 == "no-use"{print $$1}' > no-use.txt
	cat $(result) | awk '$$4 == "use"{print $$1}' > $@
