# Script to simulate Dither pattern exposure result.
#
# IMPORTANT NOTE: you will need Gnuastro 0.10.49-1d84f or newer (bug
# #57164 was found and fixed during the testing of this Makefile). The
# latest Gnuastro version is always available here:
#     http://akhlaghi.org/gnuastro-latest.tar.lz
#
# To run this script, set the "User input parameters" below, then run
# it like this:
#     $ make -f dither-simulate.mk -j8
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2019, Free Software Foundation, Inc.
#
# This Makefile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# For a copy of the GNU General Public License see
# <http://www.gnu.org/licenses/>.





# Input parameters
# ----------------
#
# These parameters depend on the host system and selected image,
# please set them manually.
#    MINIMUM-GOOD-DEPTH: The minimum acceptable depth.
#    OUT-WIDTH-PIXELS: Width of stacked output.
#    RESOLUTION-ARCSEC-PER-PIX: The pixel resolution of the input.
#    EXAMPLE-IMAGE: One single exposure image (FITS file) of the camera you
#                   want to test. The actual contents of the image is
#                   irrelevant.
#    DITHER-STEPS: a text file containing the dither steps. It should
#                  have atleast three columns:
#           Column 1: Dither ID, counting from 1.
#           Column 2: Dither's central position in X (relative to the center).
#           Column 3: Dither's central position in Y (relative to the center).
MINIMUM-GOOD-DEPTH = 21
OUT-WIDTH-PIXELS = 7001,4501
RESOLUTION-ARCSEC-PER-PIX = 3
EXAMPLE-IMAGE = an-image.fits
DITHER-STEPS = dither-steps-2.txt





# Internal Make settings
# ----------------------
#
# These are just basic Make settings to make sure things run smoothly.
all: results
.ONESHELL:
tmpdir=temporary
.SHELLFLAGS = -ec
$(tmpdir):; mkdir $@





# Single-valued image
# -------------------
#
# We will set all the input image values to 1, so their addition will
# show how many overlap. To build the flat image, we are first
# converting it to floating point and exploiting the fact that in the
# floating-point standard nothing (even NaN) will equal a NaN. So the
# ouptut of the `float32 nan eq not' operators will be a flat image
# where all the pixels will be 1.
flat=$(tmpdir)/flat.fits
$(flat): $(EXAMPLE-IMAGE) | $(tmpdir)
        # Get the coordinates of the central pixel.
	astfits $(EXAMPLE-IMAGE) -h0 \
	        | awk '/^NAXIS1/{print $$3/2}' > $(tmpdir)/center-x.txt
	astfits $(EXAMPLE-IMAGE) -h0 \
	        | awk '/^NAXIS2/{print $$3/2}' > $(tmpdir)/center-y.txt

        # Build the 1-valued image.
	astarithmetic $(EXAMPLE-IMAGE) float32 nan eq not -h0 -o$@





# Individual dithers
# ------------------
#
# We'll use Gnuastro's Crop to keep the original center in the same
# point, but shift the actual image.
dither-num := $(shell awk '!/^\#/{c++} END{print c}' $(DITHER-STEPS))
dither-ids := $(shell seq 1 $(dither-num))
dithers=$(foreach d,$(dither-ids),$(tmpdir)/$(d).fits)
$(dithers): $(tmpdir)/%.fits: $(flat)

        # Read the central pixel values and define the crop center for
        # this dither.
	cx=$$(cat $(tmpdir)/center-x.txt)
	cy=$$(cat $(tmpdir)/center-y.txt)
	center=$$(awk '$$1==$*{printf("%s,%s", '$$cx'-$$2, \
	                              '$$cy'-$$3)}' $(DITHER-STEPS))

        # Do the crop.
	crop=$(tmpdir)/$*-cropped.fits
	astcrop $(flat) --mode=img --center=$$center \
	        --width=$(OUT-WIDTH-PIXELS) -o$$crop

        # Set all the blank pixels to zero and delete the temporary
        # cropped image.
	astarithmetic $$crop set-i i i isblank 0 where -o $@
	rm $$crop





# Final exposure map
# ------------------
#
# The final exposure map is simply a sum of each dither's exposure.
cat=$(tmpdir)/cat.txt
exposure-map.fits.gz: $(dithers)

        # Merge all the exposures into one image.
	uncompressed=$(subst .gz,,$@)
	astarithmetic $(dithers) $(dither-num) sum uint8 -g1 -o$$uncompressed

        # Estimate the size of the regions, then compress the labeled image.
	astmkcatalog $$uncompressed -h1 -o$(cat) \
	             --ids --area --minx --maxx --miny --maxy
	gzip -f --best $$uncompressed




results: exposure-map.fits.gz

        # See how many pixels are above the minimum acceptable depth
	res=$(RESOLUTION-ARCSEC-PER-PIX)
	goodarea=$$(awk '$$1>=$(MINIMUM-GOOD-DEPTH){s+=$$2} \
	                 END{f='$$res'/3600;printf("%.2f",s*f*f)}' $(cat))

        # Get the size of that region. But first be careful that some
        # exposures may not actually exist in the exposure map. So
        # we'll exit AWK as soon as we print the values.
	goodsize=$$(awk '$$1>=$(MINIMUM-GOOD-DEPTH) && $$2>0{ \
	                    f='$$res'/3600; \
	                    printf("%.2f(deg) x %.2f(deg)", \
	                           ($$4-$$3)*f, ($$6-$$5)*f); \
	                    exit}' $(cat))

        # Report values:
	echo; echo; echo "Final values:"
	echo "Region with more than $(MINIMUM-GOOD-DEPTH) exposures"
	echo "Total area: $$goodarea deg^2"
	echo "Size: $$goodsize"
